﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBasicCounter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnDecrementation = New System.Windows.Forms.Button()
        Me.btnIncrementation = New System.Windows.Forms.Button()
        Me.lblValue = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.btnRAZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnDecrementation
        '
        Me.btnDecrementation.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnDecrementation.Location = New System.Drawing.Point(70, 106)
        Me.btnDecrementation.Name = "btnDecrementation"
        Me.btnDecrementation.Size = New System.Drawing.Size(75, 23)
        Me.btnDecrementation.TabIndex = 0
        Me.btnDecrementation.Text = "-"
        Me.btnDecrementation.UseVisualStyleBackColor = True
        '
        'btnIncrementation
        '
        Me.btnIncrementation.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.btnIncrementation.Location = New System.Drawing.Point(257, 106)
        Me.btnIncrementation.Name = "btnIncrementation"
        Me.btnIncrementation.Size = New System.Drawing.Size(75, 23)
        Me.btnIncrementation.TabIndex = 1
        Me.btnIncrementation.Text = "+"
        Me.btnIncrementation.UseVisualStyleBackColor = True
        '
        'lblValue
        '
        Me.lblValue.AutoSize = True
        Me.lblValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.lblValue.Location = New System.Drawing.Point(188, 102)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(23, 25)
        Me.lblValue.TabIndex = 2
        Me.lblValue.Text = "0"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.lblTotal.Location = New System.Drawing.Point(180, 53)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(40, 17)
        Me.lblTotal.TabIndex = 3
        Me.lblTotal.Text = "Total"
        '
        'btnRAZ
        '
        Me.btnRAZ.Location = New System.Drawing.Point(166, 152)
        Me.btnRAZ.Name = "btnRAZ"
        Me.btnRAZ.Size = New System.Drawing.Size(75, 23)
        Me.btnRAZ.TabIndex = 4
        Me.btnRAZ.Text = "RAZ"
        Me.btnRAZ.UseVisualStyleBackColor = True
        '
        'frmBasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(404, 273)
        Me.Controls.Add(Me.btnRAZ)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblValue)
        Me.Controls.Add(Me.btnIncrementation)
        Me.Controls.Add(Me.btnDecrementation)
        Me.Name = "frmBasicCounter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnDecrementation As Button
    Friend WithEvents btnIncrementation As Button
    Friend WithEvents lblValue As Label
    Friend WithEvents lblTotal As Label
    Friend WithEvents btnRAZ As Button
End Class
