﻿Imports BasicCounterLib

Public Class frmBasicCounter
    Private Sub btnDecrementation_Click(sender As Object, e As EventArgs) Handles btnDecrementation.Click
        lblValue.Text = Counter.Decrementation(lblValue.Text)


    End Sub

    Private Sub btnIncrementation_Click(sender As Object, e As EventArgs) Handles btnIncrementation.Click
        lblValue.Text = Counter.Incrementation(lblValue.Text)
    End Sub

    Private Sub btnRAZ_Click(sender As Object, e As EventArgs) Handles btnRAZ.Click
        lblValue.Text = Counter.RemiseAZero(lblValue.Text)

    End Sub
End Class
