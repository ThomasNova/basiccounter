﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class Counter
    {
        public int value;
 
        public static int Incrementation(int value)
        {
         
            value = value + 1;
            return value;
        }
        public static int Decrementation(int value)
        {
            if (value > 0)
            {
                value = value - 1;
            }
            else
            {
                value = 0;
            }
            return value;
        }
        public static int RemiseAZero(int value)
        {
            value = 0;
            return value;
        }

    }
}
