﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace BasicCounterTest
{
    [TestClass]
    public class CounterTest
    {
        [TestMethod]
        public void TestIncrementation()
        {
            int value = 12;
            Assert.AreEqual(13, Counter.Incrementation(value));
        }
        [TestMethod]
        public void TestDecrementation()
        {
            int value = 12;
            int value1 = -2;
            Assert.AreEqual(11, Counter.Decrementation(value));
            Assert.AreEqual(0, Counter.Decrementation(value1));
        }
        [TestMethod]
        public void TestRemiseAZero()
        {
            int value = 12;
            Assert.AreEqual(0, Counter.RemiseAZero(value));
        }
    }
}
